var check_user = require('./check_user');
var makeToken = require('./makeToken');
var verifyToken = require('../modules/verifyToken');
var addRedis = require('./redisCon').addRedis;
var checkRedis = require('./redisCon').checkRedis;
var deleteRedis = require('./redisCon').deleteRedis;

class SocketClass {


  constructor(socket){
    this.socket = socket;

    this.connectionData = {
      user: null,
      token: null,
      role: null,
      socketId: this.socket.id
    }


    this.getConUser = () =>{
      return this.connectionData.user;
    }

    this.getConToken = () =>{
      return this.connectionData.token;
    }

    this.getConSocketId = () =>{
      return this.connectionData.socketId;
    }
    this.getConRole = () =>{
      return this.connectionData.role;
    }


    console.log('SocketClass new Connection socketId: ' + this.socket.id);

    this.socket.on('userWeb', (data) => {

      checkRedis(data.username)
      .then((message) =>{
        message? (
          this.socket.emit('usertoken', {
            username: data.username,
            role: '',
            jwt: '',
            auth: false,
            message: 'User already connected'
          })
        ):(
          check_user.userok(data.username, data.password, data.ip)
            .then((role) => {
              makeToken.makeToken(data.username, role)
              .then((token) => {
                socket.emit('usertoken', {
                  username: data.username,
                  role: role,
                  jwt: token,
                  auth: true,
                  message: 'User connected'
                })

                this.connectionData.user = data.username;
                this.connectionData.token = token;
                this.connectionData.role = role;
                addRedis(this.connectionData.user, this.connectionData.token, this.connectionData.socketId)
              })
              .catch((err) => {
                this.socket.emit('usertoken', {
                  username: data.username,
                  role: role,
                  jwt: '',
                  auth: false,
                  message: err.message
                });
              })
            })
            .catch(err => {
              console.log(err);
              this.socket.emit('usertoken', {
                username: data.username,
                role: '',
                jwt: '',
                auth: false,
                message: err
              });
              // socket.disconnect();
            })
        )

      })
      .catch((err) => {
        console.log(err);
      })


    })



    this.socket.conn.on('packet', (packet) =>{
      if(packet.type === "ping"){

        if(this.connectionData.token){
          verifyToken.verifyToken(this.connectionData.token)
            .then(verifiedToken => {
              console.log('User: ' + this.getConUser() + ' socketId: ' + this.getConSocketId() + ' still connected');
              socket.emit('usertoken', {
                username: this.getConUser(),
                role: this.getConRole(),
                jwt: this.getConToken(),
                auth: true,
                message: 'User connected'
              })
            })
            .catch(err => {
              console.log('User: ' + this.getConUser() + ' not authenticated anymore')
              socket.emit('usertoken', {
                username: this.getConUser(),
                role: this.getConRole(),
                jwt: this.getConToken(),
                auth: false,
                message: 'User not connected anymore'
              })
              deleteRedis(this.getConUser())
            })
        }
      }

    })

  }




}

module.exports.SocketClass = SocketClass;
